<html>

<head>
    <title>Register</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/style/css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <?php include 'handle_register.php';?>

    <form method="post" action="register.php">
        <div class="register">
            <h1>Register</h1>
            <hr class="register__hr">
            <label for="email"><b>Email</b></label>
            <input class="register__input" type="email" placeholder="Enter Email" name="email" id="email" value="<?php echo $email; ?>" required>
            <label for="password"><b>Password</b></label>
            <input class="register__input" type="password" placeholder="Enter Password" name="password" id="password" value="<?php echo $password; ?>" required>
            <label for="password-repeat"><b>Repeat Password</b></label>
            <input class="register__input" type="password" placeholder="Repeat Password" name="password_repeat"
                id="password-repeat" required>
            <hr class="register__hr">
            <button type="submit" class="button">Register</button>
            <div class="register__signin">
                <p>Already have an account? <a href="login.php">Sign in</a>.</p>
            </div>
        </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</body>

</html>