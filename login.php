<html>

<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/style/css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <?php include 'handle_login.php';?>
    
    <form method="post" action="login.php">
        <div class="login">
            <h1>Login</h1>
            <hr class="login__hr">
            <label for="email"><b>Email</b></label>
            <input class="login__input" type="email" placeholder="Enter Email" name="email" id="email" value="<?php echo $email; ?>" required>
            <label for="password"><b>Password</b></label>
            <input class="login__input" type="password" placeholder="Enter Password" name="password" id="password" value="<?php echo $password; ?>" required>
            <hr class="login__hr">
            <button type="submit" class="button">Login</button>
            <div class="login__register">
                <p>Already haven't an account? <a href="register.php">Register now</a>.</p>
                <a href="#">Forgot password?</a>
            </div>
        </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</body>

</html>